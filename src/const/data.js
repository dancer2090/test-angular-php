export const countries = [
    {
        country_id : 1,
        country_name: 'United States'
    },
    {
        country_id : 2,
        country_name: 'Canada'
    },
    {
        country_id : 3,
        country_name: 'Mexico'
    }
];

export const states = {
    1: {
        country_name: 'United States',
        states: [
            {
                state_id: 1,
                state_name: 'Washington'
            },
            {
                state_id: 2,
                state_name: 'Florida'
            },
            {
                state_id: 3,
                state_name: 'Georgia'
            },
        ]
    },
    2: {
        country_name: 'Canada',
        states: [
            {
                state_id: 1,
                state_name: 'Quebec'
            },
            {
                state_id: 2,
                state_name: 'Nova Scotia'
            },
            {
                state_id: 3,
                state_name: 'Ontario'
            },
        ]
    },
    3: {
        country_name: 'Canada',
        states: [
            {
                state_id: 1,
                state_name: 'Aguascalientes'
            },
            {
                state_id: 2,
                state_name: 'Baja California'
            },
            {
                state_id: 3,
                state_name: 'Baja California Sur'
            },
        ]
    },
};

export const months = [
    {
        number: 1,
        name: 'January',
    },
    {
        number: 2,
        name: 'Fabruary',
    },
    {
        number: 3,
        name: 'March',
    },
    {
        number: 4,
        name: 'April',
    },
    {
        number: 5,
        name: 'May',
    },
    {
        number: 6,
        name: 'June',
    },
    {
        number: 7,
        name: 'July',
    },
    {
        number: 8,
        name: 'August',
    },
    {
        number: 9,
        name: 'September',
    },
    {
        number: 10,
        name: 'October',
    },
    {
        number: 11,
        name: 'November',
    },
    {
        number: 12,
        name: 'December',
    },
];


export const generate_years = () => {
    let current_year = Number(new Date().getFullYear());

    let years = [current_year];

    for (let i = 0; i < 10; i++) {
        current_year++;
        years.push(current_year);
        // ещё какие-то выражения
    }

    return years;
};

export const generateId = () => {
    return Math.random().toString(36).substr(2, 9);
};