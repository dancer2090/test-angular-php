'use babel';
import angular from "angular";
import routing from './app.config';
import uirouter from 'angular-ui-router';
import home from './features/home';
import  './style/app.less';

const MODULE_NAME = 'app';

// traceur.options.experimental = true;
// new traceur.WebPageTranscoder(document.location.href).run();

(function() {
    'use strict';
    angular.module(MODULE_NAME, [uirouter, home])
        .config(routing);
}());

