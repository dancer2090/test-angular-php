import {countries, states, months, generate_years, generateId} from '../../const/data';

import Visa from '../../images/visa.svg';
import Mastercard from '../../images/mastercard.svg';

const HomeController = ($scope, $http) => {

    //set default values
    $scope.regStatus = 'start';
    $scope.newCard = false;
    $scope.editCard = false;
    $scope.months = months;
    $scope.years = generate_years();
    $scope.cards = [];
    $scope.user = {};
    $scope.accept = {};
    $scope.billing = {
        asShipping: true,
        country: 1, //select united states as default
        state: 1,
        states: [],
    };
    $scope.shipping = {
        country: 1, //select united states as default
        state: 1,
        states: [],
    };
    $scope.countries = [];
    $scope.states = [];
    $scope.img = {
        visa : Visa,
        mastercard: Mastercard,
    };
    $scope.cardError = {};

    $scope.defaultCardState = () => {
        $scope.card = {
            billing : {
                asShipping: true,
                country: 1, //select united states as default
                state: 1,
                states: [],
            },
            month: 1,
            year: $scope.years[0],
        };
    };

    //send form to BE
    $scope.sendRequest  = (form) => {
        if(!$scope.accept.rules || !$scope.accept.age) {
            return false;
        }

        if(form.$invalid) {
            return false;
        }

        const data = {
            user: $scope.user,
            shipping: $scope.shipping,
            billing: $scope.billing,
            cards: $scope.cards,
        };

        $http.post('https://angular.webbuilder.in.ua/register.php', JSON.stringify(data))
        .then((response) => {
            //success
            response = response.data;

            if(response.status === 'success') {
                $scope.regStatus = 'finish';
            }

            if(response.status === 'error') {
                conosle.log($error)
            }

            console.log(response.data);
            console.log(response);
        }, (response) => {
            //error
            console.log(response);
        });
    };

    // setup counties from const value.
    $scope.getCountries = () => {
        $scope.countries = countries;
    };

    // setup states from const value.
    $scope.getStates = () => {
        if($scope.shipping.country) {
            $scope.shipping.states = states[$scope.shipping.country].states;
            $scope.shipping.state = 1;
        }
    };
    $scope.getStatesBilling = () => {
        if($scope.billing.country) {
            $scope.billing.states = states[$scope.billing.country].states;
            $scope.billing.state = 1;
        }
    };
    $scope.getStatesCardBilling = () => {
        if($scope.card.billing) {
            $scope.card.billing.states = states[$scope.card.billing.country].states;
            $scope.card.billing.state = 1;
        }
    };

    //open card form
    $scope.addNewCard = () => {
        $scope.newCard = true;
    };
    
    //close card form
    $scope.removeNewCard = () => {
        $scope.newCard = false;
        $scope.editCard = false;
    };
    //click add button in card form
    $scope.addCardToLIst = (form) => {

        if($scope.checkCardFields(form)) {
            const card = $scope.card;
            card.unique_id = generateId();
            $scope.cards.push(card);
            $scope.defaultCardState();
            $scope.removeNewCard();
        }
    };
    
    //click edit button in card form
    $scope.editCardToLIst = (selected_card) => {
        $scope.cards.map((card) => {
            if(card.unique_id == selected_card.unique_id) {
                card = selected_card;
            }
            return card;
        });
        $scope.removeNewCard();
    };

   
    $scope.checkCardFields = (form) => {
        const card = $scope.card;
        console.log(form);
        const error = [];
        if(form.cardFirstLastName.$invalid) {
            $scope.cardError.cardFirstLastName = true;
            error.push('error first_last_name');
        }

        if(form.cardNumber.$invalid ) {
            $scope.cardError.cardNumber = true;
            error.push('error card_number');
        }

        if(form.cardCVV.$invalid) {
            $scope.cardError.cardCVV = true;
            error.push('error cvv');
        }

        if(error.length > 0) {
            return false;
        } else {
            $scope.cardError = {};
            return  true;
        }
    };

    $scope.changeCardFields = (field) => {
        $scope.cardError[field] = false;
    };

    $scope.selectCard = (seleted_card) => {

        return false;
        // $scope.card = seleted_card;
        // $scope.newCard = false;
        // $scope.editCard = true;
    };

    $scope.checkAsShipping = () => {
        if($scope.billing.asShipping) {
            return false;
        } else {
            return true;
        }
    };
    $scope.checkCardAsShipping = () => {
        if($scope.card.billing.asShipping) {
            return false;
        } else {
            return true;
        }
    };

    
    //init functions
    $scope.defaultCardState();
    $scope.getCountries();
    $scope.getStatesBilling();
    $scope.getStatesCardBilling();
    $scope.getStates();

};

HomeController.$inject = ['$scope', '$http'];

export default HomeController;