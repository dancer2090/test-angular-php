routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {

    console.log($stateProvider);

    $stateProvider
        .state('home', {
            url: '/',
            template: require('./home.html'),
            controller: 'HomeController',
            controllerAs: 'home'
        })
        .state('home.login', {
            url: "/login",
            isPublic: true,
            template: require('./home.html'),
            controller: 'LoginController',
            controllerAs: 'login'
        });

}