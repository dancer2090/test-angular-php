<?php

class FormRegister {
    public $user;
    public $shipping;
    public $billing;
    public $cards;

    public $error = [];

    public function save()
    {
        //save it to database
    }

    public function validate()
    {
        // check user data
        if(isset($this->user)) {

           // check user first name
           if(isset($this->user['first_name'])) {

           } else {
               $this->error['no_user_first_name'] = 'No user first name';
           }

            //  check user last name
           if(isset($this->user['last_name'])) {

           } else {
               $this->error['no_user_last_name'] = 'No user last name';
           }

           //  check user email
           if(isset($this->user['email'])) {

           } else {
               $this->error['no_user_email'] = 'No user email';
           }

        } else {
            $this->error['no_user_data'] = 'No user data';
        }


        // check shipping data
        if(isset($this->shipping)) {

            // check shipping address
            if(isset($this->shipping['address'])) {

            } else {
                $this->error['no_shipping_address'] = 'No shipping address';
            }

            // check shipping city
            if(isset($this->shipping['city'])) {

            } else {
                $this->error['no_shipping_city'] = 'No shipping city';
            }

            // check shipping zipcode
            if(isset($this->shipping['zipcode'])) {

            } else {
                $this->error['no_shipping_zipcode'] = 'No shipping zipcode';
            }

             // check shipping country
            if(isset($this->shipping['country'])) {

            } else {
                $this->error['no_shipping_country'] = 'No shipping country';
            }

             // check shipping state
            if(isset($this->shipping['state'])) {

            } else {
                $this->error['no_shipping_state'] = 'No shipping state';
            }

        } else {
            $this->error['no_shipping_data'] = 'No shipping data';
        }
        
        // check billing data
        if(isset($this->billing)) {

            // check billing as shipping
            if(isset($this->billing['asShipping']) && $this->billing['asShipping'] === true) {
                $this->billing = $this->shipping;
            }

            // check billing address
            if(isset($this->billing['address'])) {

            } else {
                $this->error['no_billing_address'] = 'No billing address';
            }

            // check billing city
            if(isset($this->billing['city'])) {

            } else {
                $this->error['no_billing_city'] = 'No billing city';
            }

            // check billing zipcode
            if(isset($this->billing['zipcode'])) {

            } else {
                $this->error['no_billing_zipcode'] = 'No billing zipcode';
            }

             // check billing country
            if(isset($this->billing['country'])) {

            } else {
                $this->error['no_billing_country'] = 'No billing country';
            }

             // check billing state
            if(isset($this->billing['state'])) {

            } else {
                $this->error['no_billing_state'] = 'No billing state';
            }

        } else {
            $this->error['no_billing_data'] = 'No billing data';
        }


        // check billing data
        if(isset($this->cards)) {

            $cards = [];
            foreach ($this->cards as $card) {
                if(isset($card['billing']) && isset($card['billing']['asShipping']) && $card['billing']['asShipping'] === true) {
                    $card['billing'] = $this['shipping'];
                }
                array_push($cards, $card);
            }
            $this->cards = $cards;

            //additional checks here

        } else {
            $this->error['no_cards_data'] = 'No cards data';
        }


        if(count($this->error) > 0) {
            return false;
        } else {
            return true;
        }
    }
}