<?php

function checkHeaders () {
    $allowedOrigins = array(
        '(http(s)://)?(www\.)?my\-domain\.com',
        '(http(s)://)?(www\.)?localhost',
        '(http(s)://)?(www\.)?angular.webbuilder.in.ua',
    );

    if (isset($_SERVER['HTTP_ORIGIN']) && $_SERVER['HTTP_ORIGIN'] != '') {
        foreach ($allowedOrigins as $allowedOrigin) {
            if (preg_match('#' . $allowedOrigin . '#', $_SERVER['HTTP_ORIGIN'])) {
                header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
                header('Access-Control-Max-Age: 1000');
                header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
                break;
            }
        }
    }


}

function setJsonHeader() {
    header('content-type: application/json; charset=utf-8');
}