<?php

require_once ('functions.php');
require_once ('form.php');

checkHeaders();
setJsonHeader();

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    $postData = file_get_contents('php://input');
    $data = json_decode($postData, true);
    $response = [];

    $form = new FormRegister();
    $form->user = $data['user'];
    $form->shipping = $data['shipping'];
    $form->billing = $data['billing'];
    $form->cards = $data['cards'];
    if($form->validate()) {
        //send request to database here.
        $form->save();
        $response['status'] = 'success';
    } else {
        $response['status'] = ['error'];
        $response['errors'] = $form->error;
    }


    $response['sever'] = $_SERVER["CONTENT_TYPE"];

    echo json_encode($response);
}