# Test

Registration form html, css (less), angular, php

## Getting Started

1. Clone project to your computer
2. cd project-folder
3. yarn install
4. yarn dev - for development mode
5. yarn build - for production

SERVER URL - https://angular.webbuilder.in.ua/

Current project version deployed there.

## Project structure

1. src/  - working folder (js, html)
2. src/const - const scripts and functions
3. src/features - angular modules (home as default)
4. src/images - images folder
5. style - css and less folder
6. index.html - base html file
7. main.js - running script
7. server/ - php scripts (register.php - for form request)
